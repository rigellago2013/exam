<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Posts; 
use App\Comments;

class PostsController extends Controller 
{
    public function index()
    {
        $data =  \App\Posts::all();    

        return response()->json($data, 200);
    }

    public function get($id)
    {
        $data =  \App\Posts::findOrFail($id);    

        return response()->json($data, 200);
    }

    public function getComment($id)
    {
        $data =  \App\Comments::findOrFail($id);    

        return response()->json($data, 200);
    }


}